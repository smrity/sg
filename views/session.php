<?php
use App\Utility\Utility;

$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;

if(!$status) {
    Utility::redirect('login.php');
    return;
}
############################### Session time calculation #####################################
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('login.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################