<?php
	include('header.php');

?>
	<!--<div class="bcumb" style="background-image: url('resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Online Registration</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>-->
	<div class="">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="login">
						<div class="row">
								<div class="col-sm-4"></div>
								<div class="col-sm-4">
									<div class="logarea">
										<h4 class="logtit text-center" >Login</h4><hr/>
										<form class="form-horizontal" action="User/Authentication/login.php" method="post">
											<div class="form-group">
												<label for="exampleInputEmail1">Login as</label>
												<select name="loginas" class="form-control">
													<option value="Admin">Admin</option>
													<option value="User">User</option>
												</select>
											</div>
											<div class="form-group">
												<label for="exampleInputEmail1">Email address</label>
												<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">Password</label>
												<input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox"> Remember me
												</label>
											</div>
											<button type="submit" class="btn btn-default">Submit</button>
										</form>
									</div>
								</div>
								<div class="col-sm-4"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<?php
	include('footer.php');
	include('footer_script.php');

?>