<?php
	include('header.php');
?>
	<div class="bcumb" style="background-image: url('../resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>All Upcoming Events</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container upcomingevent events">
		<div class="row">
			<div class="col-md-6 left">
				<img src="../resource/img/2.jpg" alt="upcoming" class="img img-responsive"/>
				<div class="upoverlay text-center">

				</div>
			</div>
			<div class="col-md-6 right">
				<div class="area">
					<!--<center><a href="#" class="btn btn-default upcomingbtn">Upcoming Event</a></center>-->
					<div id="countdown" class="table-responsive"></div>
					<h4>We are going to arrange a get together !</h4>
					<p class="text-left">We welcome you all to the new website of The Chittagong University English Alumni Association. From now on, you will be regularly updated about the activities of the association and the English Department here. If you have received any degree, such as BA (Honours), MA, MPhil, or PhD from this department, you are one of our valued and proud alumni. We want this site to become a vibrant platform for the alumni across the globe to share their success stories which would inspire our existing students, and guide them to choose their professional pathways. Let’s grow together to serve the nation better.</p>
					<a href="" class="btn btn-default joinbtn">Join With Us</a>
				</div>
			</div>
		</div>
    </div>
	<div class="container upcomingevent events">
		<div class="row">
			<div class="col-md-6 left">
				<img src="../resource/img/bg-3.jpg" alt="upcoming" class="img img-responsive"/>
				<div class="upoverlay text-center">

				</div>
			</div>
			<div class="col-md-6 right">
				<div class="area">
					<!--<center><a href="#" class="btn btn-default upcomingbtn">Upcoming Event</a></center>-->
					<div id="countdown2" class="table-responsive"></div>
					<h4>We are going to arrange a get together !</h4>
					<p class="text-left">We welcome you all to the new website of The Chittagong University English Alumni Association. From now on, you will be regularly updated about the activities of the association and the English Department here. If you have received any degree, such as BA (Honours), MA, MPhil, or PhD from this department, you are one of our valued and proud alumni. We want this site to become a vibrant platform for the alumni across the globe to share their success stories which would inspire our existing students, and guide them to choose their professional pathways. Let’s grow together to serve the nation better.</p>
					<a href="" class="btn btn-default joinbtn">Join With Us</a>
				</div>
			</div>
		</div>
    </div>
	
<?php
	include('footer.php');
?>