 <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="footer-content">
              <div class="ql">


              </div>
              
              <div class="footer-icons">

              </div>
            </div>
          </div>
         <div class="col-md-3">
            <div class="footer-content">



            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-content">

            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-content">

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Skygoal</strong>. All Rights Reserved | <a href="#">Privacy Policy</a>
              </p>
            </div>
            <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eBusiness
              -->
              Designed by <a target="_blank" title="Visit Oline IT" href="http://olineit.com/">Oline IT</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!--multiform-->
   <script type="text/javascript">
      
  </script>
  <!--multiform-->


  <!--owl slider by rana-->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/owl.carousel.min.js'></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.owl-carousel').owlCarousel({
    stagePadding: 200,
    loop:true,
    margin:10,
    nav:false,
    items:1,
    lazyLoad: true,
    autoplay: true,
    nav:true,
      responsive:{
            0:{
                items:1,
                stagePadding: 60
            },
            600:{
                items:1,
                stagePadding: 100
            },
            1000:{
                items:1,
                stagePadding: 200
            },
            1200:{
                items:1,
                stagePadding: 250
            },
            1400:{
                items:1,
                stagePadding: 300
            },
            1600:{
                items:1,
                stagePadding: 350
            },
            1800:{
                items:1,
                stagePadding: 400
            }
        }
    })
    
    });
  </script>
  <!-- JavaScript Libraries -->
  <script src="../resource/lib/jquery/jquery.min.js"></script>
  <!--photogall js-->
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
  <!--photogall js-->
  

  <script src="../resource/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="../resource/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../resource/lib/venobox/venobox.min.js"></script>
  <script src="../resource/lib/knob/jquery.knob.js"></script>
  <script src="../resource/lib/wow/wow.min.js"></script>
  <script src="../resource/lib/parallax/parallax.js"></script>
  <script src="../resource/lib/easing/easing.min.js"></script>
  <script src="../resource/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="../resource/lib/appear/jquery.appear.js"></script>
  <script src="../resource/lib/isotope/isotope.pkgd.min.js"></script>



  <script src="../resource/js/main.js"></script>
</body>

</html>
