<?php
	include('header.php');

?>
	<!--<div class="bcumb" style="background-image: url('resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Online Registration</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>-->
	<div class="">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="contact">
						<div class="row">
							<div class="col-md-4">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3154.0121201366765!2d91.78688855861316!3d22.471077642134258!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30acd6fe9a3a4473%3A0x7836276aef538552!2sUniversity+of+Chittagong!5e1!3m2!1sen!2sbd!4v1545893674756" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="col-md-8">
								<div class="conarea">
									<h4 class="ctit">Send Message</h4>
									<form class="" method="POST" action="#"> 
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="name">Name</label>
													<input type="text" class="form-control" id="name" placeholder="Name">
												</div>
											</div>
											<div class="col-md-6">
													<div class="form-group">
														<label for="email">Email address</label>
														<input type="email" class="form-control" id="email" placeholder="Email">
													</div>
											</div>
										</div>
										<div class="form-group">
											<label for="subject">Subject</label>
											<input type="text" class="form-control" id="subject" placeholder="Subject">
										</div>
										<div class="form-group">
											<label for="message">Message</label>
											<textarea cols="3" rows="3" class="form-control" id="message" placeholder="Message"></textarea>
										</div>
									  
									  <button type="submit" name="send" id="send" class="btn btn-default">Send</button>
									</form>
									<?php
										if(isset($_POST['send']))
										{
											$name     = $_POST['name'];
											$email    = $_POST['email'];
											$subject  = $_POST['subject'];
											$message  = $_POST['message'];
											
											// the email will be sent here
											// make sure to change this to be your e-mail
											$to= "info@cuenglishalumni.com";
											$header=$email;
											$header2="From: $email\r\nReply-To: $email\r\nReturn-Path: $email\r\n";

											// the email subject
											// '[Contact Form] :' will appear automatically in the subject.
											// You can change it as you want

											$subject = ' ' . $subject;

											// the mail message ( add any additional information if you want )
											$msg     = "From : $name \r\n E-mail : $email  \r\nSubject : $subject  \r\n\n" . "Message : \r\n$message";
											$email_sent=mail($to, $subject, $msg, $header2 );
											if($email_sent){?>
											<?php
												echo "
													<div class=''>
														<p style='text-align:center; color:black;'><span>Thank you!&nbsp; <b>".$name."</b> <br />&nbsp;your message has sent. Thanks for stay with us.</span></p>
													</div>
												";
												
											}
											
										}
										?>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<?php
	include('footer.php');
?>