
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('../resource/img/abroad-education-consultants.jpg')">
            <div class="carousel-caption text-left">
				<div class="container">
					<h1>We Are Proud</h1>
					<h4>Students Of <span>Chittagong University</span></h4>
					<p>An alumni association is an association of graduates or, more broadly, of former students (alumni).</p>
					<div class="btnarea">
						<a href="#" class="btn btn-default mission">Our Mission</a>
						<a href="#" class="btn btn-default story">Our Story</a>
					</div>
				</div>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('../resource/img/abroad-education-consultants.jpg')">


          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('../resource/img/banner3.jpg')">
            <div class="carousel-caption text-left">
				<div class="container">
					<h2>We Are Alumni</h2>
					<h4>Department Of <span>English</span></h4>
					<p>We are going to arrange our 4th Reunion !</p>
					<div class="btnarea">
						<a href="registration.php" class="btn btn-default mission">Register Now</a>
						<a href="life_registration.php" class="btn btn-default story">Get Life Membership</a>
					</div>
				</div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>
