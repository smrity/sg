<?php
	include('header.php');
?>
	<div class="bcumb" style="background-image: url('../resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Gallery</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>
	<div class="Gallery">
		<div class="container text-center">
			<!--<div align="center" id="garea">
				<ul>
					<li class="active"><a class="btn btn-default filter-button current" data-filter="all">All</a></li>
					<li><a class="btn btn-default filter-button" data-filter="hdpe">Old Memories</a></li>
					<li><a class="btn btn-default filter-button" data-filter="sprinkle">Event</a></li>
					<li><a class="btn btn-default filter-button" data-filter="spray">Our Picnic</a></li>
					<li><a class="btn btn-default filter-button" data-filter="irrigation">Recent</a></li>
				</ul>
			</div>-->
			<div align="center">
				<button class="btn btn-default filter-button current" data-filter="all">All</button>
				<button class="btn btn-default filter-button" data-filter="hdpe">Old Memories</button>
				<button class="btn btn-default filter-button" data-filter="sprinkle">Event</button>
				<button class="btn btn-default filter-button" data-filter="spray">Our Picnic</button>
				<button class="btn btn-default filter-button" data-filter="irrigation">recent</button>
			</div>
			
			<br/>
			<div class="row">
				<div class="gallery_product col-md-4 filter hdpe">
					<a href="../resource/img/gallery/a.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/a.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-4 filter sprinkle">
					<a href="../resource/img/gallery/b.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/b.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-4 filter hdpe">
					<a href="../resource/img/gallery/c.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/c.jpg" class="img-responsive">
					</a>
				</div>
			</div>
			<div class="row">
				<div class="gallery_product col-md-6 filter irrigation">
					<a href="../resource/img/gallery/d.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/d.jpg" class="img-responsive">
					</a>
				</div>

				<div class="gallery_product col-md-6 filter spray">
					<a href="../resource/img/gallery/e.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/e.jpg" class="img-responsive">
					</a>
				</div>

			</div>
			<div class="row">
				<div class="gallery_product col-md-6 filter hdpe">
					<a href="../resource/img/gallery/f.jpg" data-toggle="lightbox" data-gallery="gallery">
						<img src="../resource/img/gallery/f.jpg" class="img-responsive">
					</a>
				</div>

			</div>
		</div>
	</div>
	
<?php
	include('footer.php');
?>