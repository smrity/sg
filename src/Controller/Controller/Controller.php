<?php
namespace App\Controller;
use App\Message\Message;
use App\Utility\Utility;
use App\Rbac\Role;
use App\Rbac\PrivilegedUser;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Controller extends  DB{

 private $asfor,$fileNo,$refEm,$refSkygoal,$consltName,$Applycountry,$toWork,$surName,$givenName,$fullName,$fathersName,$mothersName,$maritalStatus,$dob,$age,$pzcode,$pob,$citizenship,$passportNo,$passportNoOld,$placeOfissue,$passportIssuedate,$passportExpirydate,$NID,$permAddress1,$permAddress2,$post_office,$zip_code,$state,$country,$degree_name1,$group1,$board1,$passingYear1,$result1,$companyName,$companyAddress,$typeOfwork,$workDuration,$months,$comEmail,$comWebsite,$fatherName, $motherName,$addressLine1, $addressLine2, $city,$postcode,$email,$phoneNumber,$gender,$tov, $passport, $birthcert, $passport_file, $picture, $brc, $cv, $comment, $tmpPass, $singleView,$deleteid;

    public function setData($postData){

        if(array_key_exists('asfor',$postData)){$this->asfor=$postData['asfor'];}
        if(array_key_exists('fileNo',$postData)){$this->fullName=$postData['fileNo'];}
        if(array_key_exists('refEm',$postData)){$this->fullName=$postData['refEm'];}
        if(array_key_exists('refSkygoal',$postData)){$this->fullName=$postData['refSkygoal'];}
        if(array_key_exists('consltName',$postData)){$this->fullName=$postData['consltName'];}
        if(array_key_exists('Applycountry',$postData)){$this->fullName=$postData['Applycountry'];}
        if(array_key_exists('toWork',$postData)){$this->fullName=$postData['toWork'];}
        if(array_key_exists('surName',$postData)){$this->fullName=$postData['surName'];}
        if(array_key_exists('givenName',$postData)){$this->fullName=$postData['givenName'];}
        if(array_key_exists('fullName',$postData)){$this->fullName=$postData['fullName'];}
        if(array_key_exists('fathersName',$postData)){$this->fullName=$postData['fathersName'];}
        if(array_key_exists('mothersName',$postData)){$this->fullName=$postData['mothersName'];}
        if(array_key_exists('maritalStatus',$postData)){$this->fullName=$postData['maritalStatus'];}
        if(array_key_exists('dob',$postData)){$this->fullName=$postData['dob'];}
        if(array_key_exists('age',$postData)){$this->fullName=$postData['age'];}
        if(array_key_exists('pzcode',$postData)){$this->fullName=$postData['pzcode'];}
        if(array_key_exists('pob',$postData)){$this->fullName=$postData['pob'];}
        if(array_key_exists('citizenship',$postData)){$this->fullName=$postData['citizenship'];}
        if(array_key_exists('passportNo',$postData)){$this->fullName=$postData['passportNo'];}
        if(array_key_exists('passportNoOld',$postData)){$this->fullName=$postData['passportNoOld'];}
        if(array_key_exists('placeOfissue',$postData)){$this->fullName=$postData['placeOfissue'];}
        if(array_key_exists('passportIssuedate',$postData)){$this->fullName=$postData['passportIssuedate'];}
        if(array_key_exists('passportExpirydate',$postData)){$this->fullName=$postData['passportExpirydate'];}
        if(array_key_exists('NID',$postData)){$this->fullName=$postData['NID'];}
        if(array_key_exists('permAddress1',$postData)){$this->fullName=$postData['permAddress1'];}
        if(array_key_exists('permAddress2',$postData)){$this->fullName=$postData['permAddress2'];}
        if(array_key_exists('post_office',$postData)){$this->fullName=$postData['post_office'];}
        if(array_key_exists('zip_code',$postData)){$this->fullName=$postData['zip_code'];}
        if(array_key_exists('state',$postData)){$this->fullName=$postData['state'];}
        if(array_key_exists('country',$postData)){$this->fullName=$postData['country'];}
        if(array_key_exists('degree_name1',$postData)){$this->fullName=$postData['degree_name1'];}
        if(array_key_exists('group1',$postData)){$this->fullName=$postData['group1'];}
        if(array_key_exists('board1',$postData)){$this->fullName=$postData['board1'];}
        if(array_key_exists('passingYear1',$postData)){$this->fullName=$postData['passingYear1'];}
        if(array_key_exists('result1',$postData)){$this->fullName=$postData['result1'];}
        if(array_key_exists('maritalStatus',$postData)){$this->fullName=$postData['maritalStatus'];}
        if(array_key_exists('companyName',$postData)){$this->fullName=$postData['companyName'];}
        if(array_key_exists('companyAddress',$postData)){$this->fullName=$postData['companyAddress'];}
        if(array_key_exists('typeOfwork',$postData)){$this->fullName=$postData['typeOfwork'];}
        if(array_key_exists('workDuration',$postData)){$this->fullName=$postData['workDuration'];}
        if(array_key_exists('months',$postData)){$this->fullName=$postData['months'];}
        if(array_key_exists('comEmail',$postData)){$this->fullName=$postData['comEmail'];}
        if(array_key_exists('comWebsite',$postData)){$this->fullName=$postData['comWebsite'];}


        if(array_key_exists('fullName',$postData)){$this->fullName=$postData['fullName'];}
        if(array_key_exists('fatherName',$postData)){$this->fatherName =$postData['fatherName']; }
        if(array_key_exists('motherName',$postData)){$this->motherName = $postData['motherName']; }
        if(array_key_exists('addressLine1',$postData)){$this->addressLine1 = $postData['addressLine1']; }
        if(array_key_exists('addressLine2',$postData)){ $this->addressLine2 = $postData['addressLine2']; }
        if(array_key_exists('city',$postData)){ $this->city = $postData['city'];}
        if(array_key_exists('state',$postData)){$this->state = $postData['state'];}
        if(array_key_exists('postcode',$postData)){$this->postcode = $postData['postcode'];}
        if(array_key_exists('country',$postData)){$this->country = $postData['country'];}
        if(array_key_exists('email',$postData)){$this->email = $postData['email'];}
        if(array_key_exists('phoneNumber',$postData)){$this->phoneNumber = $postData['phoneNumber'];}
        if(array_key_exists('dob',$postData)){$this->dob = $postData['dob'];}
        if(array_key_exists('gender',$postData)){$this->gender = $postData['gender'];}
        if(array_key_exists('tov',$postData)){$this->tov = $postData['tov'];}
        if(array_key_exists('passport',$postData)){$this->passport= $postData['passport'];}
        if(array_key_exists('birthcert',$postData)){$this->birthcert = $postData['birthcert'];}
        if(array_key_exists('passport_file',$postData)){$this->passport_file = $postData['passport_file'];}
        if(array_key_exists('picture',$postData)){$this->picture = $postData['picture'];}
        if(array_key_exists('brc',$postData)){$this->brc = $postData['brc'];}
        if(array_key_exists('cv',$postData)){$this->cv = $postData['cv'];}
        if(array_key_exists('comments',$postData)){$this->comment = $postData['comments'];}
        if(array_key_exists('singleView',$postData)){$this->singleView = $postData['singleView'];}
        if(array_key_exists('deleteid',$postData)){$this->deleteid = $postData['deleteid'];}

    }
    public function objectToArray($objectData){
        $objectToArray = json_decode(json_encode($objectData), True);
        return $objectToArray;
    }

    public function generate_string($input, $strength = 16){
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
     }
    
    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+';
        $pass = array(); //remember to declare $pass as an array
        $tmpPass=$pass;
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function sms($mobileNumber,$message){

        ################ Getting SMS settings from DB ####################################
        $sql="SELECT * FROM settings WHERE name='sms'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $smsSettings=$STH->fetch();
        $objToArraySmsSet = json_decode(json_encode($smsSettings), True);
        ################ Getting SMS settings from DB Ended ###############################
        $user = $objToArraySmsSet['username'];
        $password = $objToArraySmsSet['password'];
        $sender = $objToArraySmsSet['senderid'];
        $url = "http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$message&GSM=$mobileNumber&";
        $smsResult = simplexml_load_file($url);
        return $smsResult;

    }

    public function store(){

            $Pass=$this->randomPassword();

            $this->tmpPass=$Pass;

            $this->password=md5($Pass);


             $arrData = array($this->password,$this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->email,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO members(password,fullName,fatherName, motherName,addressLine1, addressLine2, city, state, postcode, country,email, phoneNumber, dob, gender,tov, passport, birthcert, passport_file, picture, brc, cv, comment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("Success! You have  successfully registered:)");
       }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

    }
    public function assstore(){

            $Pass=$this->randomPassword();

            $this->tmpPass=$Pass;

            $this->password=md5($Pass);


             $arrData = array($this->password,$this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->email,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO members(password,fullName,fatherName, motherName,addressLine1, addressLine2, city, state, postcode, country,email, phoneNumber, dob, gender,tov, passport, birthcert, passport_file, picture, brc, cv, comment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("Success! You have  successfully registered:)");
       }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

    }

    public function fileUpload($postData,$filesData){
    if(isset($postData)){
    if(count($_FILES['fileToUpload']['name']) > 0){
        $totalFiles=count($_FILES['fileToUpload']['name']);
        for($i=0; $i<$totalFiles; $i++) {
            $tmpFilePath = $_FILES['fileToUpload']['tmp_name'][$i];
            if($tmpFilePath != ""){
                 $shortname = time().$_FILES['fileToUpload']['name'][$i];
                if($i==0){
                    $_POST['passport_file']="passport".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['passport_file']; }
                if($i==1){
                    $_POST['picture']="picture".$_POST['phoneNumber'].$shortname;
                    $fileName=$_POST['picture']; }
                if($i==2){
                    $_POST['brc']="brc".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['brc']; }
                if($i==3){
                    $_POST['cv']="cv".$_POST['phoneNumber']. $shortname;
                    $fileName=$_POST['cv']; }
                 $filePath = "uploads/" .$fileName;
                if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $files[] = $shortname;
                    $listFile=count($files);
                }
            }
        }
        if($listFile){
            $this->setData($_POST);
            if($_POST['update']=='update'){
                $this->update();
                Utility::redirect('home.php');
            }else{
                $this->store();
                Utility::redirect('email.php?fullName='.$this->fullName.'&password='.$this->tmpPass.'&email='.$this->email.'&sendmail=ok');
            }


        }else{
            unset($files,$listFile); return True;
        }

        }

    }
}

    public function view() {

        $sql = "";
        $sql = "SELECT * FROM  members";
      if($_SESSION['loginas']=='User' || $this->singleView=='Yes'){
          $sql = "SELECT * FROM  members WHERE email='$this->email'";
      }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function update(){
        $arrData = array($this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        //
        //
        $sql = "UPDATE members SET  fullName=?,fatherName=?, motherName=?,addressLine1=?, addressLine2=?, city=?, state=?, postcode=?, country=?, phoneNumber=?, dob=?, gender=?,tov=?, passport=?, birthcert=?, passport_file=?, picture=?, brc=?, cv=?, comment=? WHERE email='$this->email'";

        //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success!");
        else
            Message::message("Failed!");

        Utility::redirect('home.php');
    }


    public function delete(){

        $sql = "Delete from members  WHERE id=".$this->deleteid;
        $result = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");
        Utility::redirect('home.php');
    }

}