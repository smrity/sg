<?php
namespace App\Controller;
use App\Message\Message;
use App\Utility\Utility;
use App\Rbac\Role;
use App\Rbac\PrivilegedUser;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Controller extends  DB{

    private $asfor,$fileNo,$refEm,$refSkygoal,$consltName,$Applycountry,$toWork,$surName,$givenName,$fullName,$fathersName,$mothersName,$maritalStatus,$dob,$age,$pzcode,$pob,$citizenship,$passportNo,$passportNoOld,$placeOfissue,$passportIssuedate,$passportExpirydate,$NID,$permAddress1,$permAddress2,$post_office,$zip_code,$state,$country,$degree_name1,$group1,$board1,$passingYear1,$result1,$companyName,$companyAddress,$typeOfwork,$workDuration,$months,$comEmail,$comWebsite, $singleView,$fatherName, $motherName,$addressLine1, $addressLine2, $city, $postcode,$email, $phoneNumber,$gender,$tov, $passport, $birthcert, $passport_file, $picture, $brc, $cv, $comment, $tmpPass;

    public function setData($postData){
        //var_dump($postData);
        if(array_key_exists('fullName',$postData)){$this->fullName=$postData['fullName'];}
        if(array_key_exists('fatherName',$postData)){$this->fatherName =$postData['fatherName']; }
        if(array_key_exists('motherName',$postData)){$this->motherName = $postData['motherName']; }
        if(array_key_exists('addressLine1',$postData)){$this->addressLine1 = $postData['addressLine1']; }
        if(array_key_exists('addressLine2',$postData)){ $this->addressLine2 = $postData['addressLine2']; }
        if(array_key_exists('city',$postData)){ $this->city = $postData['city'];}
        if(array_key_exists('state',$postData)){$this->state = $postData['state'];}
        if(array_key_exists('postcode',$postData)){$this->postcode = $postData['postcode'];}
        if(array_key_exists('country',$postData)){$this->country = $postData['country'];}
        if(array_key_exists('email',$postData)){$this->email = $postData['email'];}
        if(array_key_exists('phoneNumber',$postData)){$this->phoneNumber = $postData['phoneNumber'];}
        if(array_key_exists('dob',$postData)){$this->dob = $postData['dob'];}
        if(array_key_exists('gender',$postData)){$this->gender = $postData['gender'];}
        if(array_key_exists('tov',$postData)){$this->tov = $postData['tov'];}
        if(array_key_exists('passport',$postData)){$this->passport= $postData['passport'];}
        if(array_key_exists('birthcert',$postData)){$this->birthcert = $postData['birthcert'];}
        if(array_key_exists('passport_file',$postData)){$this->passport_file = $postData['passport_file'];}
        if(array_key_exists('picture',$postData)){$this->picture = $postData['picture'];}
        if(array_key_exists('brc',$postData)){$this->brc = $postData['brc'];}
        if(array_key_exists('cv',$postData)){$this->cv = $postData['cv'];}
        if(array_key_exists('comments',$postData)){$this->comment = $postData['comments'];}
        if(array_key_exists('singleView',$postData)){$this->singleView = $postData['singleView'];}

    }
    public function objectToArray($objectData){
        $objectToArray = json_decode(json_encode($objectData), True);
        return $objectToArray;
    }

    public function generate_string($input, $strength = 16){
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+';
        $pass = array(); //remember to declare $pass as an array
        $tmpPass=$pass;
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function sms($mobileNumber,$message){

        ################ Getting SMS settings from DB ####################################
        $sql="SELECT * FROM settings WHERE name='sms'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $smsSettings=$STH->fetch();
        $objToArraySmsSet = json_decode(json_encode($smsSettings), True);
        ################ Getting SMS settings from DB Ended ###############################
        $user = $objToArraySmsSet['username'];
        $password = $objToArraySmsSet['password'];
        $sender = $objToArraySmsSet['senderid'];
        $url = "http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$message&GSM=$mobileNumber&";
        $smsResult = simplexml_load_file($url);
        return $smsResult;

    }

    public function store(){

        $Pass=$this->randomPassword();

        $this->tmpPass=$Pass;

        $this->password=md5($Pass);


        $arrData = array($this->password,$this->fullName,$this->fatherName,$this->motherName,$this->addressLine1,$this->addressLine2,$this->city,$this->state,$this->postcode,$this->country,$this->email,$this->phoneNumber,$this->dob,$this->gender,$this->tov,$this->passport,$this->birthcert,$this->passport_file,$this->picture,$this->brc,$this->cv,$this->comment);
        //var_dump( $arrData) ; die();

        $sql = "INSERT INTO members(password,fullName,fatherName, motherName,addressLine1, addressLine2, city, state, postcode, country,email, phoneNumber, dob, gender,tov, passport, birthcert, passport_file, picture, brc, cv, comment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("Success! You have  successfully registered:)");
        }
        else
            Message::message("Failed! Data Has Not Been Inserted 😞 ");

    }


    public function index(){
        $sql = "select * from members where soft_delete='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function fileUpload($postData,$filesData){
        if(isset($postData)){
            if(count($_FILES['fileToUpload']['name']) > 0){
                $totalFiles=count($_FILES['fileToUpload']['name']);
                for($i=0; $i<$totalFiles; $i++) {
                    $tmpFilePath = $_FILES['fileToUpload']['tmp_name'][$i];
                    if($tmpFilePath != ""){
                        $shortname = time().$_FILES['fileToUpload']['name'][$i];
                        if($i==0){
                            $_POST['passport_file']="passport".$_POST['phoneNumber']. $shortname;
                            $fileName=$_POST['passport_file']; }
                        if($i==1){
                            $_POST['picture']="picture".$_POST['phoneNumber'].$shortname;
                            $fileName=$_POST['picture']; }
                        if($i==2){
                            $_POST['brc']="brc".$_POST['phoneNumber']. $shortname;
                            $fileName=$_POST['brc']; }
                        if($i==3){
                            $_POST['cv']="cv".$_POST['phoneNumber']. $shortname;
                            $fileName=$_POST['cv']; }
                        $filePath = "uploads/" .$fileName;
                        if(move_uploaded_file($tmpFilePath, $filePath)) {
                            $files[] = $shortname;
                            $listFile=count($files);
                        }
                    }
                }
                if($listFile){
                    $this->setData($_POST);
                    $this->store();
                    Utility::redirect('email.php?fullName='.$this->fullName.'&password='.$this->tmpPass.'&email='.$this->email.'&sendmail=ok');
                }else{
                    unset($files,$listFile); return True;
                }
                Utility::redirect('login.php');
            }

        }
    }

    public function view(){
//echo $this->singleView;
        $sql = "";
        $sql = "SELECT * FROM  members";
        if($_SESSION['loginas']=='User' || $this->singleView=='Yes'){
            $sql = "SELECT * FROM  members WHERE email='$this->email'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }

    public function update(){
        $arrData = array($this->picture,$this->fullName,$this->phoneNumber,$this->dob);
        //
        //var_dump($arrData);
        $sql = "UPDATE members SET picture=?,fullName=?,phoneNumber=?,dob=? WHERE id=".$this->id;
        //var_dump($this->DBH);die();
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success!");
        else
            Message::message("Failed!");

        Utility::redirect('index.php');
    }

    public function delete(){
        // var_dump($_GET);die();
        $sql = "Delete from members WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);
        if($result)
            Message::message("Success!");
        else
            Message::message("Failed!");

        Utility::redirect('index.php');
    }

}