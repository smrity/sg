<?php
namespace App\Synchronize;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Import extends  DB{

    private $modifiedDate, $categoryid, $studentName,$fatherName,$motherName,$class,$session,$batch,$birthDate,$mobile,$nid,$email,$remarks;
    private $allData;
    public function setData($postData){

        if(array_key_exists('modifiedDate',$postData)){
            $this->modifiedDate = $postData['modifiedDate'];
        }
        if(array_key_exists('studentName',$postData)){
            $this->studentName = $postData['studentName'];
        }
        if(array_key_exists('fatherName',$postData)){
            $this->fatherName = $postData['fatherName'];
        }
         if(array_key_exists('motherName',$postData)){
            $this->motherName = $postData['motherName'];
        }
         if(array_key_exists('class',$postData)){
            $this->class = $postData['class'];
        }
        if(array_key_exists('session',$postData)){
            $this->session = $postData['session'];
        }
         if(array_key_exists('batch',$postData)){
            $this->batch = $postData['batch'];
        }
        if(array_key_exists('birthDate',$postData)){
            $this->birthDate = $postData['birthDate'];
        }
        if(array_key_exists('mobile',$postData)){
            $this->mobile = $postData['mobile'];
        }
        if(array_key_exists('nid',$postData)){
            $this->nid = $postData['nid'];
        }
        if(array_key_exists('email',$postData)){
            $this->email= $postData['email'];
        }
        if(array_key_exists('wastageValue',$postData)){
            $this->wastageValue= $postData['wastageValue'];
        }
        if(array_key_exists('remarks',$postData)){
            $this->remarks= $postData['remarks'];
        }

    }
    public function store(){

 //var_dump($_POST); die();
        $arrData = array($this->modifiedDate,$this->studentName,$this->fatherName,$this->motherName,$this->class,$this->session,$this->batch,$this->birthDate,$this->mobile,$this->nid,$this->email,$this->remarks);
        $sql = "INSERT into student(created,studentName,fatherName,motherName,class,session,batch,birthDate,mobile,nid,email,remarks) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData);


        if($result){
                Message::message("Success! Data Has Been Inserted Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('index.php');
    }

    public function import(){

        //$sql = "select * from salesentry where salesentry.soft_deleted='No'";
        $sql="SELECT * from expenseincome ";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData=$STH->fetchAll();
        $result="";
        foreach ($allData as $singleData){

            if($singleData->transactionType==1){
                $arrData = array("5","MPAY",$singleData->transactionDate,$singleData->receiptInvoiceNo,$singleData->PayerPayee,$singleData->amount,$singleData->particulars);
                $sql = "INSERT into transactionentries(branchid,transactionType,transactionDate,receiptInvoiceNo,paidto,amountOut,remarks) VALUES(?,?,?,?,?,?,?)";
                $STH = $this->DBH->prepare($sql);
                $result =$STH->execute($arrData);
            }
            else{
                $arrData = array("5","MREC",$singleData->transactionDate,$singleData->receiptInvoiceNo,$singleData->PayerPayee,$singleData->amount,$singleData->particulars);
                $sql = "INSERT into transactionentries(branchid,transactionType,transactionDate,receiptInvoiceNo,receivedFrom,amountIn,remarks) VALUES(?,?,?,?,?,?,?)";
                $STH = $this->DBH->prepare($sql);
                $result =$STH->execute($arrData);
            }

        }

        if($result){
            Message::message("Success! Data Has Been Imported Successfully :)");
        }
        else
            Message::message("Failed! Data Has Not Been Imported :( ");

        Utility::redirect('../index.php');

    }


}